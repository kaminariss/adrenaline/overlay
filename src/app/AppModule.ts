import { NgModule }                     from '@angular/core';
import { FormsModule }                  from '@angular/forms';
import { BrowserModule }                from '@angular/platform-browser';
import { RouterModule, Routes }         from '@angular/router';
import { NgSelectModule }               from '@ng-select/ng-select';
import { ColorSketchModule }            from 'ngx-color/sketch';
import { ConfigWindowBarComponent }     from 'src/app/Component/Config/ConfigWindowBarComponent';
import { ConfigWindowBarTextComponent } from 'src/app/Component/Config/ConfigWindowBarTextComponent';
import { ConfigWindowColorComponent }   from 'src/app/Component/Config/ConfigWindowColorComponent';
import { ConfigWindowMainComponent }    from 'src/app/Component/Config/ConfigWindowMainComponent';
import { ConfigWindowProfileComponent } from 'src/app/Component/Config/ConfigWindowProfileComponent';
import { ConfigButtonComponent }        from 'src/app/Component/Config/Ui/ConfigButtonComponent';
import { ConfigCheckboxComponent }      from 'src/app/Component/Config/Ui/ConfigCheckboxComponent';
import { ConfigColorComponent }         from 'src/app/Component/Config/Ui/ConfigColorComponent';
import { ConfigGroupComponent }         from 'src/app/Component/Config/Ui/ConfigGroupComponent';
import { ConfigInputComponent }         from 'src/app/Component/Config/Ui/ConfigInputComponent';
import { ConfigMultiselectComponent }   from 'src/app/Component/Config/Ui/ConfigMultiselectComponent';
import { ConfigPositionComponent }      from 'src/app/Component/Config/Ui/ConfigPositionComponent';
import { ConfigRangeComponent }         from 'src/app/Component/Config/Ui/ConfigRangeComponent';
import { ConfigSelectComponent }        from 'src/app/Component/Config/Ui/ConfigSelectComponent';
import { ConfigSizeComponent }          from 'src/app/Component/Config/Ui/ConfigSizeComponent';
import { ConfigTextWidgetComponent }    from 'src/app/Component/Config/Ui/ConfigTextWidgetComponent';
import { ConfigComponent }              from 'src/app/Component/ConfigComponent';
import { ProgressBarComponent }         from 'src/app/Component/ProgressBarComponent';
import { TextWidget }                   from 'src/app/Component/TextWidget';
import { WindowPanelComponent }         from 'src/app/Component/WindowPanelComponent';
import { IconClose }                    from 'src/app/Icon/IconClose';
import { IconCog }                      from 'src/app/Icon/IconCog';
import { BarContainerComponent }        from 'src/app/Main/BarContainerComponent';
import { HeaderBarComponent }           from 'src/app/Main/HeaderBarComponent';
import { StatusBarComponent }           from 'src/app/Main/StatusBarComponent';
import { ConfigService }                from 'src/app/Service/ConfigService';
import { MainComponent }                from './Main/MainComponent';
import { SettingsComponent }            from './Settings/SettingsComponent';
import { AppComponent }                 from './AppComponent';

const routes: Routes = [
	{
		path: '',
		component: MainComponent
	},
	{
		path: 'settings',
		component: SettingsComponent
	}
];

export function getBaseUrl() {
	return document.getElementsByTagName('base')[0].href;
}

@NgModule({
	declarations: [
		AppComponent,
		MainComponent,
		SettingsComponent,

		BarContainerComponent,
		StatusBarComponent,
		HeaderBarComponent,

		ProgressBarComponent,
		TextWidget,

		IconClose,
		IconCog,

		ConfigButtonComponent,
		ConfigCheckboxComponent,
		ConfigColorComponent,
		ConfigGroupComponent,
		ConfigInputComponent,
		ConfigMultiselectComponent,
		ConfigPositionComponent,
		ConfigRangeComponent,
		ConfigSelectComponent,
		ConfigSizeComponent,
		ConfigTextWidgetComponent,

		WindowPanelComponent,
		ConfigComponent,
		ConfigWindowMainComponent,
		ConfigWindowBarComponent,
		ConfigWindowBarTextComponent,
		ConfigWindowProfileComponent,
		ConfigWindowColorComponent
	],
	imports: [
		BrowserModule,
		RouterModule.forRoot(routes),
		FormsModule,
		ColorSketchModule,
		NgSelectModule
	],
	providers: [
		{ provide: 'BASE_URL', useFactory: getBaseUrl }
	],
	bootstrap: [AppComponent]
})
export class AppModule {
}
