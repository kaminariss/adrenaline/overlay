import { Component }     from '@angular/core';
import { ConfigService } from 'src/app/Service/ConfigService';
import { barDirections } from 'src/app/Data/barDirections';
import { barStyles }     from 'src/app/Data/barStyles';

@Component({
	selector: 'config-bar',
	template: `
		<config-group title="Bar Configuration">
			<config-checkbox [configObj]="config" prop="useJobColor" label="Use Job Color"></config-checkbox>
			<config-color [configObj]="config" prop="barColor" label="Bar Color"></config-color>
			<config-color [configObj]="config" prop="bgColor" label="Background"></config-color>
			<config-input [configObj]="config" prop="height" label="Bar Height"></config-input>
			<config-input inputType="number" [configObj]="config" prop="margin" label="Spacing"></config-input>
			<config-select [configObj]="config" [items]="barStyles" prop="barStyle" label="Bar Style"></config-select>
			<config-select [configObj]="config" [items]="barDirections" prop="barDirection" label="Bar Direction"></config-select>
		</config-group>
	`
})
export class ConfigWindowBarComponent {
	barStyles = barStyles;
	barDirections = barDirections;

	config = this.conf.config.barConfig;

	constructor(public conf: ConfigService) {}

	reload() {
		window.location.reload();
	}

	resetConfig(prop: string) {
		this.conf.resetConfig(prop, '');
	}

	closeConfig() {
		window.close();
	}

	resetAll() {
		this.conf.resetAllConfig();
	}
}