import { Component }     from '@angular/core';
import { ConfigService } from 'src/app/Service/ConfigService';

@Component({
	selector: 'config-bar-text',
	template: `
		<config-text-widget title="Name label"
			[configObj]="config.widgets.name"
		></config-text-widget>
		
		<config-text-widget title="Dps label"
			[configObj]="config.widgets.dps"
		></config-text-widget>

		<config-text-widget title="Total label"
			[configObj]="config.widgets.total"
		></config-text-widget>
		
		<config-text-widget title="Job label"
			[configObj]="config.widgets.job"
		></config-text-widget>
	`
})
export class ConfigWindowBarTextComponent {
	config = this.conf.config;

	constructor(public conf: ConfigService) {}
}