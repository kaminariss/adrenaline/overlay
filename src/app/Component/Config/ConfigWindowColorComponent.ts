import { Component }     from '@angular/core';
import { GameData }      from 'src/app/Model/GameData';
import { ConfigService } from 'src/app/Service/ConfigService';

@Component({
	selector: 'config-window-color',
	template: `
		<config-group title="Job Colors">
			<config-color *ngFor="let j of jobs"
				[customSet]="true"
				[label]="j.job"
				(getProp)="getProp($event, j.jobId)"
				(setProp)="setProp($event, j.jobId)"
				(resetProp)="resetProp(j.jobId)"
			></config-color>
		</config-group>
	`
})
export class ConfigWindowColorComponent {
	jobs = GameData.jobsArray();

	config = this.conf.config;

	constructor(public conf: ConfigService) {}

	closeConfig() {
		window.close();
	}

	getProp($event: {value: any}, jobId: number) {
		$event.value = this.config.colorConfig.getJobColor(jobId);
	}

	setProp($event: any, jobId: number) {
		this.config.colorConfig.setJobColor(jobId, $event);
	}

	resetProp(jobId: number) {
		this.config.colorConfig.resetJobColor(jobId);
	}
}