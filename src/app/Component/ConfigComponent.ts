import { Component, Inject } from '@angular/core';
import { ConfigService }     from 'src/app/Service/ConfigService';
import { default as config } from '../../../package.json';

@Component({
	selector: 'config-window',
	template: `
		<ng-content></ng-content>

		<window-panel (close)="closeConfig()" style="font-family: 'Segoe UI', sans-serif">
			<ng-container header>
				<img [src]="logoUrl" style="width: 39px; background: #fff;" alt="Adrenaline">
				<h4 class="mt-1 ms-2">
					Adrenaline - Config <small class="text-muted">(v{{ version }})</small>
				</h4>
			</ng-container>

			<ng-container pane>
				<div class="btn-group-vertical w100p">
					<button class="btn btn-sm"
						*ngFor="let cat of categories"
						[ngClass]="currentCategory === cat.value ? 'btn-dark bg-light text-dark' : 'btn-outline-dark text-light'"
						(click)="switchWindow(cat.value)">
						{{ cat.label }}
					</button>
				</div>
			</ng-container>

			<config-main *ngIf="currentCategory === 'main'"></config-main>
			<config-bar *ngIf="currentCategory === 'bar'"></config-bar>
			<config-bar-text *ngIf="currentCategory === 'bar-text'"></config-bar-text>
			<config-window-color *ngIf="currentCategory === 'color'"></config-window-color>
			<config-window-profile *ngIf="currentCategory === 'profile'"></config-window-profile>
		</window-panel>
	`
})
export class ConfigComponent {
	version = config.version;
	logoUrl = this.baseUrl + 'assets/logo.png';

	categories = [
		{ value: 'main', label: 'General settings' },
		{ value: 'bar', label: 'Bars' },
		{ value: 'bar-text', label: 'Bar Texts' },
		{ value: 'color', label: 'Colors' },
		{ value: 'profile', label: 'Profiles' }
	];

	currentCategory = 'main';

	constructor(
		public conf: ConfigService,
		@Inject('BASE_URL') protected baseUrl: string
	) {}

	switchWindow(pane: string) {
		this.currentCategory = pane;
	}

	closeConfig() {
		window.close();
	}
}