import { RecursivePartial } from 'src/app/Interface/RecursivePartial';
import { MainConfig }       from 'src/app/Model/Config/MainConfig';

export const miniConfig: RecursivePartial<MainConfig> = {
	version: 2,
	fontFamily: 'Inter',
	webFont: 'https://fonts.googleapis.com/css2?family=Inter&display=swap',
	customCss: '.progress-bar-fill {\nbackground-image: linear-gradient(rgba(50, 50, 50, 0.1), rgba(200, 200, 200, 0.3));\n}',
	blurNames: false,

	barConfig: {
		useJobColor: true,
		barColor: 'red',
		bgColor: 'black',
		barStyle: 'horizontal',
		height: '20px',
		barDirection: 'start',
		margin: 0
	},

	widgets: {
		name: {
			show: true,
			anchor: 'left',
			fontColor: '#fff',
			fontSize: '14px',
			position: { x: 35, y: 0 }
		},
		dps: {
			show: true,
			anchor: 'center',
			fontColor: '#fff',
			fontSize: '14px',
			position: { x: 0, y: 0 }
		},
		total: {
			show: true,
			anchor: 'right',
			fontColor: '#fff',
			fontSize: '14px',
			position: { x: 5, y: 0 }
		},
		job: {
			show: true,
			anchor: 'left',
			fontColor: '#fff',
			fontSize: '14px',
			position: { x: 5, y: 0 }
		}
	},

	colorConfig: {
		enabled: true
		// default values are in class itself
	}
};