export interface DpsEntry {
	objectId: number;
	ownerId: number;
	jobId: number;
	companyTag: string;
	name: string;
	perSecond: number;
	total: number;
	percentage: number;
}