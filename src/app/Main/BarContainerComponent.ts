import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { GameData }                             from 'src/app/Model/GameData';
import { ConfigService }                        from 'src/app/Service/ConfigService';
import { DpsChart }                             from 'src/app/Service/DpsChart';

@Component({
	selector: 'bar-container',
	template: `
		<progress-bar class="bar"
			*ngFor="let entry of chart.list.value"
			[height]="config.height"
			[percent]="entry.percentage"
			[bgColor]="config.bgColor"
			[barDirection]="config.barDirection"
			[barStyle]="config.barStyle"
			[fillColor]="barColor(entry.jobId)"
			[style.margin-bottom.px]="config.margin"
		>
			<div class="position-absolute z10 text-widget-name"
				text-widget
				[config]="widgetCfg.name"
				[class.blur-3]="cfg.config.blurNames"
			>
				{{ entry.name }}
			</div>
			
			<div class="position-absolute z10 text-widget-dps"
				text-widget
				[config]="widgetCfg.dps"
			>
				{{ entry.perSecond.toFixed(1) }}
			</div>
			
			<div class="position-absolute z10 text-widget-total"
				text-widget
				[config]="widgetCfg.total"
			>
				{{ entry.total.toFixed(1) }}
			</div>
			
			<div class="position-absolute z10 text-widget-job text-widget-job-{{ job(entry.jobId) }}"
				text-widget
				[config]="widgetCfg.job"
			>
				{{ job(entry.jobId) }}
			</div>
		</progress-bar>
	`
})
export class BarContainerComponent implements OnInit {
	config = this.cfg.config.barConfig;
	widgetCfg = this.cfg.config.widgets;

	job = GameData.jobEnumToJob;

	constructor(
		public chart: DpsChart,
		protected cd: ChangeDetectorRef,
		public cfg: ConfigService
	) {}

	ngOnInit() {
		this.cd.detach();
		this.chart.list.subscribe(this.updateBars.bind(this));
	}

	updateBars() {
		this.cd.detectChanges();
	}

	barColor(jobId: number) {
		if (!jobId) {
			return this.config.barColor;
		}
		else {
			return this.config.useJobColor ?
				this.cfg.config.colorConfig.getJobColor(jobId) : this.config.barColor;
		}

	}
}