import { Component } from '@angular/core';
import { default as config } from '../../../package.json';

@Component({
	selector: 'header-bar',
	template: `
		<div class="d-flex" style="line-height: 30px">
			<div class="flex-fill">
				<img src="assets/fav.png" alt="" 
					class="ms-1"
					style="vertical-align: -2px;">DRENALINE {{ version }}
			</div>
			<icon-cog (click)="openSettings()" class="me-1"></icon-cog>
		</div>
	`
})
export class HeaderBarComponent {
	settingsUrl = window.location.href + '/settings';
	version = config.version;

	openSettings() {
		const w = 800;
		const h = 600;
		const y = window.top.outerHeight / 2 + window.top.screenY - (h / 2);
		const x = window.top.outerWidth / 2 + window.top.screenX - (w / 2);
		return window.open(
			this.settingsUrl,
			'adrenaline-config',
			`toolbar=no, location=no, directories=no, status=no, menubar=no, copyhistory=no, width=${ w }, height=${ h }, top=${ y }, left=${ x }`
		);
	}
}