import { Component, OnInit } from '@angular/core';
import { DpsChart }          from 'src/app/Service/DpsChart';
import { TestService }       from 'src/app/Service/TestService';
import { XivService }        from 'src/app/Service/XivApi/XivService';

@Component({
	templateUrl: 'MainComponent.html'
})
export class MainComponent implements OnInit {
	xiv = new XivService();

	constructor(
		public chart: DpsChart,
		public t: TestService
	) {}

	ngOnInit() {
		const connected = this.xiv.initialize();
		if (!connected) {
			console.log('Unable to connect to socket');
		}
		this.xiv.events.message.subscribe(this.onMessage.bind(this));
	}

	onMessage(response: any) {
		if (response.event === 'dps') {//objectId: 276732064, perSecond: 9.42310945352264e-9, total: 601
			console.log(
				'DPS', response.data[0]
			);

			this.chart.update(response.data);

			// this.entries = response.data;
			// if (this.entries.length !== response.data.length) {
			// 	this.entries = response.data;
			// } else {
			//
			// }
		}
	}

}