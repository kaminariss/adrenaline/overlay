import { BarDirection }            from 'src/app/Interface/BarDirection';
import { BarStyle }                from 'src/app/Interface/BarStyle';
import { BaseConfig }              from 'src/app/Model/Config/BaseConfig';
import { DistinctBehaviorSubject } from 'src/app/Model/DistinctBehaviorSubject';

export class BarConfig extends BaseConfig {
	// @formatter:off
	get useJobColor(): boolean { return this.useJobColorSub.value; }
	set useJobColor(v: boolean) { this.useJobColorSub.next(v); }
	useJobColorSub = new DistinctBehaviorSubject<boolean>(true);

	get barColor(): string { return this.barColorSub.value; }
	set barColor(v: string) { this.barColorSub.next(v); }
	barColorSub = new DistinctBehaviorSubject<string>('');

	get bgColor(): string { return this.bgColorSub.value; }
	set bgColor(v: string) { this.bgColorSub.next(v); }
	bgColorSub = new DistinctBehaviorSubject<string>('');

	get height(): string { return this.heightSub.value; }
	set height(v: string) { this.heightSub.next(v); }
	heightSub = new DistinctBehaviorSubject<string>('20px');

	get margin(): number { return this.marginSub.value; }
	set margin(v: number) { this.marginSub.next(v); }
	marginSub = new DistinctBehaviorSubject<number>(0);

	get barStyle(): BarStyle { return this.barStyleSub.value; }
	set barStyle(v: BarStyle) { this.barStyleSub.next(v); }
	barStyleSub = new DistinctBehaviorSubject<BarStyle>('horizontal');

	get barDirection(): BarDirection { return this.barDirectionSub.value; }
	set barDirection(v: BarDirection) { this.barDirectionSub.next(v); }
	barDirectionSub = new DistinctBehaviorSubject<BarDirection>('start');
	// @formatter:on
}