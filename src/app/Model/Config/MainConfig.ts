import { BehaviorSubject, merge, Observable, Subject } from 'rxjs';
import { debounceTime }                                from 'rxjs/operators';
import { getSubjects }                                 from 'src/app/Function/getSubjects';
import { serialize }                                   from 'src/app/Function/serialize';
import { unserialize }                                 from 'src/app/Function/unserialize';
import { ClassLike }                                   from 'src/app/Interface/ClassLike';
import { SerializableConfig }                          from 'src/app/Interface/SerializableConfig';
import { BarConfig }                                   from 'src/app/Model/Config/BarConfig';
import { ColorConfig }                                 from 'src/app/Model/Config/ColorConfig';
import { TextWidgetConfig }                            from 'src/app/Model/Config/TextWidgetConfig';
import { DistinctBehaviorSubject }                     from 'src/app/Model/DistinctBehaviorSubject';

export class MainConfig implements SerializableConfig {
	version = 1;

	// @formatter:off
	get fontFamily(): string { return this.fontFamilySub.value; }
	set fontFamily(v: string) { this.fontFamilySub.next(v); }
	fontFamilySub = new DistinctBehaviorSubject<string>('');

	get webFont(): string { return this.webFontSub.value; }
	set webFont(v: string) { this.webFontSub.next(v); }
	webFontSub = new DistinctBehaviorSubject<string>('');

	get customCss(): string { return this.customCssSub.value; }
	set customCss(v: string) { this.customCssSub.next(v); }
	customCssSub = new DistinctBehaviorSubject<string>('');

	get blurNames(): boolean { return this.blurNamesSub.value; }
	set blurNames(v: boolean) { this.blurNamesSub.next(v); }
	blurNamesSub = new DistinctBehaviorSubject<boolean>(false);

	get replaceYourName(): string { return this.replaceYourNameSub.value; }
	set replaceYourName(v: string) { this.replaceYourNameSub.next(v); }
	replaceYourNameSub = new DistinctBehaviorSubject<string>('');
	// @formatter:on

	colorConfig = new ColorConfig();
	barConfig = new BarConfig();

	widgets = {
		name: new TextWidgetConfig(),
		job: new TextWidgetConfig(),
		dps: new TextWidgetConfig(),
		total: new TextWidgetConfig(),
	}

	anyChangedCache: Observable<any>;

	get anyChanged(): Observable<any> {
		this.anyChangedCache ??= merge(...this.getSubjects()).pipe(debounceTime(10));
		return this.anyChangedCache;
	};

	getSubjects(): Subject<any>[] {
		return getSubjects(this);
	}

	serialize(): any {
		return serialize(this);
	}

	unserialize(value: any): void {
		value = this.upgradeConfig(value);
		unserialize(this, value);
	}

	protected upgradeConfig(cfg: ClassLike<MainConfig>) {
		return cfg;
	}

}