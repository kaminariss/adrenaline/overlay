import { Injectable, Renderer2, RendererFactory2 } from '@angular/core';
import { BehaviorSubject, merge, Subject }         from 'rxjs';
import { debounceTime }                            from 'rxjs/operators';
import { miniConfig }                              from 'src/app/Data/Config/Profiles/miniConfig';
import { ConfigProfile }                           from 'src/app/Interface/ConfigProfile';
import { MainConfig }                              from 'src/app/Model/Config/MainConfig';
import { DistinctBehaviorSubject }                 from 'src/app/Model/DistinctBehaviorSubject';
import extend                                      from 'just-extend';
import { WebFontService }                          from 'src/app/Service/WebFontService';

@Injectable({ providedIn: 'root' })
export class ConfigService {
	defaultConfig = miniConfig;

	profiles: ConfigProfile[] = [];
	config: MainConfig;

	acceptFocus = new BehaviorSubject<boolean>(false);
	renderer: Renderer2;

	configChanged = new Subject();
	configKey = 'adrenalineConfig';

	constructor(
		protected rendererFactory: RendererFactory2,
		protected webFontService: WebFontService
	) {
		(window as any).configService = this;

		this.renderer = rendererFactory.createRenderer(null, null);

		this.loadProfiles();
		this.loadConfig();
		this.applyConfig();
	}

	loadConfig() {
		let c: any = localStorage.getItem(this.configKey) || '';
		try {
			c = JSON.parse(c);
		}
		catch (e) {
			c = {};
		}

		const config = extend(true, this.cloneDefaultConfig(), c) as any;

		this.config = new MainConfig();
		this.config.unserialize(config);

		const subs: BehaviorSubject<any>[] = [];
		this.findObservers(this.config, subs);

		merge(...subs)
			.pipe(debounceTime(10))
			.subscribe(() => {
				this.applyConfig();
			});
	}

	reloadConfig() {
		let c: any = localStorage.getItem(this.configKey) || '';
		try {
			c = JSON.parse(c);
		}
		catch (e) {
			c = {};
		}

		const config = extend(true, this.cloneDefaultConfig(), c) as any;

		this.config.unserialize(config);
	}

	cloneDefaultConfig() {
		return extend(true, {}, this.defaultConfig);
	}

	findObservers(obj: any, subs: BehaviorSubject<any>[]) {
		if (typeof obj !== 'object') {
			return;
		}

		if (obj.anyChanged) {
			subs.push(obj.anyChanged);
		}

		for (const k in obj) {
			if (!obj.hasOwnProperty(k)) {
				continue;
			}

			const val = obj[k];
			if (val instanceof DistinctBehaviorSubject || val instanceof BehaviorSubject) {
				continue;
			}

			if (typeof val === 'object') {
				this.findObservers(val, subs);
			}
		}
	}

	resetAllConfig() {
		const config = this.cloneDefaultConfig() as any;
		this.config.unserialize(config);
	}

	resetConfig(prop: string, configPath: string) {
		let obj = this.config as any;
		let defObj = this.defaultConfig as any;

		const exp = configPath.split('.').filter(n => n);
		while (exp.length > 0) {
			const k = exp.shift();
			defObj = defObj[k];
			obj = obj[k];
		}

		obj[prop] = defObj[prop];
	}

	applyConfig() {
		localStorage.setItem(this.configKey, JSON.stringify(this.config.serialize()));

		console.log('config saved', this.config);

		document.body.style.fontFamily = this.config.fontFamily;
		this.webFontService.loadWebfont(this.config.webFont);
		this.configChanged.next(true);
		this.setCustomCss();

		if (window.opener && (window.opener as any).configService) {
			const parentCfg = (window.opener as any).configService;
			console.log('we are settings window');
			parentCfg.reloadConfig();
		}
	}

	setCustomCss() {
		document.getElementById('custom-css-container').innerText = this.config.customCss;
	}

	createNewProfile(newProfileName: string) {
		const serialized = JSON.stringify(this.config.serialize());
		const profiles = this.profiles;
		profiles.push({ name: newProfileName, config: serialized });

		this.profiles = [...profiles];

		this.saveProfiles();
	}

	deleteProfile(profileName: string) {
		const profileIdx = this.profiles.findIndex(p => p.name === profileName);
		if (profileIdx < 0) {
			console.log(`Profile ${ profileName } not found!`);
			return;
		}

		const profiles = this.profiles.splice(profileIdx, 1);
		this.profiles = [...profiles];

		this.saveProfiles();
	}

	loadProfile(profileName: string) {
		const profile = this.profiles.find(p => p.name === profileName);
		if (!profile) {
			console.log(`Profile ${ profileName } not found!`);
			return;
		}

		try {
			const c = JSON.parse(profile.config);
			const config = extend(true, this.cloneDefaultConfig(), c) as any;
			this.config.unserialize(config);
		}
		catch (e) {
			console.log(e);
		}
	}

	saveProfiles() {
		localStorage.setItem('profiles', JSON.stringify(this.profiles));
	}

	loadProfiles() {
		const profiles = localStorage.getItem('profiles');
		if (profiles) {
			const parsed = JSON.parse(profiles);
			if (Array.isArray(parsed)) {
				this.profiles = parsed;
			}
		}
		else {
			this.resetProfiles();
		}
	}

	resetProfiles() {
		this.profiles = [
			{ name: 'Mini', config: JSON.stringify(miniConfig) }
		];
		this.saveProfiles();
	}

	saveAsProfile(profileName: string) {
		const profile = this.profiles.find(p => p.name === profileName);
		if (!profile) {
			console.log(`Profile ${ profileName } not found!`);
			return;
		}

		profile.config = JSON.stringify(this.config.serialize());

		this.saveProfiles();
	}

	async exportProfile(profileName: string) {
		const profile = this.profiles.find(p => p.name === profileName);
		if (!profile) {
			console.log(`Profile ${ profileName } not found!`);
			return '';
		}

		return profile.config;
	}

	async importProfile(profileName: string, input: string) {
		const profile = this.profiles.find(p => p.name === profileName);
		if (!profile) {
			console.log(`Profile ${ profileName } not found!`);
			return;
		}

		profile.config = input;
	}
}