import { Injectable }      from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { DpsEntry }        from 'src/app/Interface/DpsEntry';

@Injectable({ providedIn: 'root' })
export class DpsChart {
	list = new BehaviorSubject<DpsEntry[]>([]);
	combatTime: number = 0;
	mergePets = true;
	excludeNpcs = true;

	static isPlayer(id: number) {
		return id.toString(16).startsWith('1');
	}

	update(entries: DpsEntry[]) {
		if (this.excludeNpcs) {
			entries = entries.filter(e => {
				return DpsChart.isPlayer(e.objectId) || DpsChart.isPlayer(e.ownerId);
			});
		}

		if (this.mergePets) {
			const newEntries = [];
			for (const e of entries) {
				if (!e.ownerId || e.ownerId === 0xE0000000) {
					newEntries.push(e);
					continue;
				}

				const parent = entries.find(p => p.objectId === e.ownerId);
				if (parent) {
					parent.total += e.total;
					parent.perSecond += e.perSecond;
				}
			}
			entries = newEntries;
		}

		entries.sort((a: DpsEntry, b: DpsEntry) => {
			return b.total - a.total;
		});

		let totalMax = 0;
		if (entries.length > 0) {
			totalMax = entries[0].total;
		}

		for (const e of entries) {
			e.percentage = 100 * e.total / totalMax;
		}

		this.list.next(entries);
	}
}