import { Injectable } from '@angular/core';
import { DpsEntry }   from 'src/app/Interface/DpsEntry';
import { DpsChart }   from 'src/app/Service/DpsChart';

@Injectable({ providedIn: 'root' })
export class TestService {
	interval: number;
	timeRunning = 0;
	period = 500;

	constructor(
		public chart: DpsChart
	) {
		(window as any).testService = this;
	}

	stopTicking() {
		if (this.interval) {
			window.clearInterval(this.interval);
		}
	}

	startTicking() {
		this.stopTicking();

		this.generateInitial();

		this.interval = window.setInterval(this.generateNext.bind(this), this.period);
	}

	generateInitial() {
		const amount = this.randomRange(3, 8);
		const pets = this.randomRange(1, 3);

		let entries: DpsEntry[] = [];
		for (let i = 0; i < amount; i++) {
			entries.push({
				name: (Math.random() + 1).toString(36).substring(7),
				percentage: 0,
				total: 0,
				perSecond: 0,
				ownerId: 0,
				objectId: 0x10000000 + this.randomRange(1, 1000),
				jobId: this.randomRange(1, 40),
				companyTag: ''
			});
		}

		this.chart.update(entries);
	}

	generateNext() {
		this.timeRunning += this.period;

		const entries = this.chart.list.value;
		for (const e of entries) {
			e.total += this.randomRange(10, 300);
			e.perSecond = e.total * 1000 / this.timeRunning;
		}

		this.chart.update(entries);
	}

	// Utils
	randomElement(arr: any[], filter?: (value: any) => boolean) {
		if (filter) {
			arr = arr.filter(filter);
		}
		return arr[Math.floor(Math.random() * arr.length)];
	}

	randomRange(min: number, max: number) {
		return Math.floor(Math.random() * (max - min + 1) + min);
	}
}