import { Subject }                from 'rxjs';
import { Conditions }             from './Interface/Conditions';
import { StatusFlags }            from './Interface/StatusFlags';
import { ActionEffect1 }          from './Interface/ActionEffect';
import { AppliedStatus }          from './Interface/AppliedStatus';
import { CrossWorldPartyMember }  from './Interface/CrossWorldPartyMember';
import { UpdatePositionInstance } from './Interface/UpdatePositionInstance';
import { Actor }                  from './Interface/Actor';
import { ActorCast }              from './Interface/ActorCast';
import { ActorChangedEvent }      from './Interface/ActorChangedEvent';
import { ActorMove }              from './Interface/ActorMove';
import { ActorSetPos }            from './Interface/ActorSetPos';
import { ChatMessageEvent }       from './Interface/ChatMessageEvent';
import { EffectResult }           from './Interface/EffectResult';
import { EffectResultBasic }      from './Interface/EffectResultBasic';
import { NetworkEvent }           from './Interface/NetworkEvent';
import { NpcSpawn }               from './Interface/NpcSpawn';
import { ObjectDespawn }          from './Interface/ObjectDespawn';
import { PartyMember }            from './Interface/PartyMember';
import { PlayerSpawn }            from './Interface/PlayerSpawn';
import { TargetChangedEvent }     from './Interface/TargetChangedEvent';
import { UpdateHpMpTp }           from './Interface/UpdateHpMpTp';
import { UpdatePosition }         from './Interface/UpdatePosition';

import { ActorControl, ActorControlSelf, ActorControlTarget }     from './Interface/ActorControl';
import { StatusEffectList, StatusEffectList2, StatusEffectList3 } from './Interface/StatusEffectList';

// noinspection JSUnusedGlobalSymbols
export class XivService {
	socket: WebSocket;
	port = 32805;

	connected = false;

	events = {
		message: new Subject<any>(),
		playerLogin: new Subject<Actor>(),
		playerLogout: new Subject<any>(),

		chatMessage: new Subject<ChatMessageEvent>(),
		actorChanged: new Subject<ActorChangedEvent>(),

		targetChanged: new Subject<TargetChangedEvent>(),
		targetOfTargetChanged: new Subject<TargetChangedEvent>(),
		focusChanged: new Subject<TargetChangedEvent>(),

		uiVisibilityChanged: new Subject<boolean>(),

		actorCast: new Subject<NetworkEvent<ActorCast>>(),
		actorMove: new Subject<NetworkEvent<ActorMove>>(),
		actorSetPos: new Subject<NetworkEvent<ActorSetPos>>(),
		actorControl: new Subject<NetworkEvent<ActorControl>>(),
		actorControlSelf: new Subject<NetworkEvent<ActorControlSelf>>(),
		actorControlTarget: new Subject<NetworkEvent<ActorControlTarget>>(),
		objectDespawn: new Subject<NetworkEvent<ObjectDespawn>>(),
		playerSpawn: new Subject<NetworkEvent<PlayerSpawn>>(),
		npcSpawn: new Subject<NetworkEvent<NpcSpawn>>(),
		updateHpMpTp: new Subject<NetworkEvent<UpdateHpMpTp>>(),
		effectResult: new Subject<NetworkEvent<EffectResult>>(),
		effectResultBasic: new Subject<NetworkEvent<EffectResultBasic>>(),
		updatePosition: new Subject<NetworkEvent<UpdatePosition>>(),
		updatePositionInstance: new Subject<NetworkEvent<UpdatePositionInstance>>(),

		actionEffect1: new Subject<NetworkEvent<ActionEffect1>>(),
		statusEffectList: new Subject<NetworkEvent<StatusEffectList>>(),
		statusEffectList2: new Subject<NetworkEvent<StatusEffectList2>>(),
		statusEffectList3: new Subject<NetworkEvent<StatusEffectList3>>(),

		zoneChanged: new Subject<number>(),
		partyChanged: new Subject<{ currentParty: PartyMember[], partyLeader: number }>(),
		crossWorldPartyChanged: new Subject<{ currentParty: CrossWorldPartyMember[] }>(),
		enmityListChanged: new Subject<Actor[]>(),
		conditionChanged: new Subject<{ condition: keyof Conditions, value: boolean }>(),
		statusFlagsChanged: new Subject<StatusFlags>()
	};

	async initialize() {
		const connected = await this.connect();
		if (!connected) {
			return false;
		}

		return connected;
	}

	toPascalCase(str: string) {
		return str
			.split(' ')
			.map(word => word[0].toUpperCase().concat(word.slice(1)))
			.join(' ');
	}

	async subscribeEvents(events: string[]) {
		const response = await this.doRequest('subscribeTo', {
			events: events
		});

		console.log('Events subscribed', response);
	}

	connect(): Promise<boolean> {
		return new Promise<boolean>(resolve => {
			if (this.socket) {
				this.socket.close();
			}

			try {
				this.socket = new WebSocket(`ws://127.0.0.1:${ this.port }/ws`);
				this.socket.addEventListener('open', () => {
					this.onOpen();
					resolve(true);
				});

				this.socket.addEventListener('close', this.onClose.bind(this));
				this.socket.addEventListener('error', () => {
					resolve(false);
					this.onError();
				});
				this.socket.addEventListener('message', this.onMessage.bind(this));
			}
			catch (e) {
				console.log(e);
				resolve(false);
			}
		});
	}

	generateGuid() {
		return 'AO' + Math.random().toString(36).substring(2, 9);
	}

	async getPlayer(): Promise<Actor> {
		return await this.doRequest('getPlayer');
	}

	async getConditions(): Promise<Conditions> {
		return await this.doRequest('getConditions');
	}

	async getStatusFlags(): Promise<Conditions> {
		return await this.doRequest('getStatusFlags');
	}

	async getParty(): Promise<{ currentParty: PartyMember[], partyLeader: number }> {
		return await this.doRequest('getParty');
	}

	async getCrossWorldParty(): Promise<{ currentParty: CrossWorldPartyMember[] }> {
		return await this.doRequest('getCrossWorldParty');
	}

	async watchActor(id: number) {
		await this.doRequest('watchActor', { id });
	}

	async watchActors(ids: number[]) {
		for (const id of ids) {
			await this.watchActor(id);
		}
	}

	async unwatchActor(id: number) {
		await this.doRequest('unwatchActor', { id });
	}

	async unwatchActors(ids: number[]) {
		for (const id of ids) {
			await this.watchActor(id);
		}
	}

	async examine(id: number) {
		await this.doRequest('examine', { id });
	}

	async sendTell(id: number) {
		await this.doRequest('sendTell', { id });
	}

	async resetEnmity(id: number) {
		await this.doRequest('resetEnmity', { id });
	}

	/**
	 * Trade is only possible with target
	 */
	async trade() {
		await this.doRequest('tradeRequest');
	}

	async promote(id: number) {
		await this.doRequest('promotePartyMember', { id });
	}

	async kick(id: number) {
		await this.doRequest('kickFromParty', { id });
	}

	/**
	 * Can only invite target
	 */
	async invite() {
		await this.doRequest('inviteToParty');
	}

	async follow() {
		await this.doRequest('followTarget');
	}

	async meldRequest() {
		await this.doRequest('meldRequest');
	}

	async disbandParty() {
		await this.doRequest('disbandParty');
	}

	async showEmoteWindow() {
		await this.doRequest('showEmoteWindow');
	}

	async showSignsWindow() {
		await this.doRequest('showSignsWindow');
	}

	async leaveParty() {
		await this.doRequest('leaveParty');
	}

	/**
	 * Zero to clear target/focus/mouseOver
	 */
	async setTarget(id: number) {
		await this.doRequest('setTarget', { id });
	}

	async setFocus(id: number) {
		await this.doRequest('setFocus', { id });
	}

	async setMouseOver(id: number) {
		await this.doRequest('setMouseOverEx', { id });
	}

	async clearMouseOver() {
		await this.doRequest('clearMouseOverEx');
	}

	setAcceptFocus(accept: boolean) {
		if (!this.connected) {
			return;
		}

		this.socket.send(JSON.stringify({
			guid: this.generateGuid(),
			type: 'setAcceptFocus',
			accept
		}));
	}

	async getActors(): Promise<Actor[]> {
		return await this.doRequest('getActors');
	}

	async getActor(id: number) {
		return await this.doRequest('getActor', { id });
	}

	async getActorStatuses(id: number): Promise<AppliedStatus[]> {
		return await this.doRequest('getActorStatuses', { id });
	}

	doRequest(type: string, data: any = {}, timeout = 2000): Promise<any> {
		return new Promise<any>(resolve => {
			if (!this.connected) {
				resolve(null);
				return null;
			}

			let tim: number;
			const guid = this.generateGuid();
			const oneTime = (event: any) => {
				try {
					const response = JSON.parse(event.data);
					if (typeof response.data?.success === 'boolean' && !response.data?.success) {
						console.log('REQUEST FAILED');
						console.log(data);
						console.log(response);
					}
					if (response.guid === guid) {
						clearTimeout(tim);
						resolve(response.data);
						this.socket.removeEventListener('message', oneTime);
					}
				}
				catch (e) {
					console.log(e);
				}
			};

			this.socket.addEventListener('message', oneTime);
			let dataToSend = {
				guid: guid,
				type: type
			};
			if (data) {
				dataToSend = Object.assign(dataToSend, { request: data });
			}

			this.socket.send(JSON.stringify(dataToSend));
			tim = window.setTimeout(() => {
				resolve(null);
				this.socket.removeEventListener('message', oneTime);
			}, timeout);
		});
	}

	onOpen() {
		console.log('XiVPlugin connected');
		this.connected = true;
	}

	onClose() {
		console.log('XiVPlugin disconnected');
		this.connected = false;
	}

	onError() {
		console.log('XiVPlugin error');
		this.connected = false;
	}

	onMessage(event: any) {
		try {
			const response = JSON.parse(event.data);
			if (response.event) {
				const ev = (this.events as any)[response.event] as Subject<any>;
				if (ev) {
					ev.next(response.data);
				}
				this.events.message.next(response);
			}
		}
		catch (e) {
			console.log(e);
		}
	}
}